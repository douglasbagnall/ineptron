from distutils.core import setup
from distutils.extension import Extension
import numpy as np

setup(
    name = "ineptron",
    ext_modules = [
        Extension(
            name="ineptron",
            library_dirs=['.'],
            sources=["ineptron-numpy.c", "ineptron.c", "pbm-dump.c", "colour.c"],
            depends=["ineptron.h"],
            include_dirs = [".", np.get_include()],
            define_macros = [('_GNU_SOURCE', None)],
            extra_compile_args = ['-march=native', '-ggdb', '-std=gnu11',
                                  '-I.', '-Wall', '-O3', '-ffast-math',
                                  '-fno-inline', '-DVECTOR', '-DPIC',
            ],
            language="c"
        )
    ]
)
