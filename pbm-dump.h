
void pbm_dump(const uint64_t *data64,
	      const uint32_t width,
	      const uint32_t height,
	      const char *name);
