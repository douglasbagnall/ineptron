/* Copyright (C) 2018 Douglas Bagnall <douglas@halo.gen.nz> */
#ifndef _GOT_BITS_H
#define _GOT_BITS_H 1
#include <stdio.h>

#define ROTATE(x, k) (((x) << (k)) | ((x) >> (sizeof(x) * 8 - (k))))

/* memory allocation. 16 for SSE, 32 for  */
#define ALIGNMENT 64

#ifndef __clang__
#define ASSUME_ALIGNED(x)   (x) = __builtin_assume_aligned ((x), ALIGNMENT)
#else
#define ASSUME_ALIGNED(x) /* x */
#endif

#define ALIGNMENT_BYTES ((size_t)ALIGNMENT / 8ULL)
#define ALIGNMENT_MASK ~(ALIGNMENT_BYTES - 1ULL)

#define ASSUME_ALIGNED_SIZE(x, type)					\
	(((x) * sizeof(type) ==						\
	 (((x) * sizeof(type)) & ALIGNMENT_MASK)) ?			\
	 (x) : __builtin_unreachable())

#define ROUND_UP_ALIGNED(x) (((x) + ALIGNMENT_BYTES - 1ULL) & ALIGNMENT_MASK)
#define ROUND_UP_ALIGNED_BITS(x) ROUND_UP_ALIGNED((x + 7) / 8)

#define ALIGNED_SIZEOF(x) ROUND_UP_ALIGNED(sizeof(x))

#define ALIGNED_VECTOR_LEN(x, type) \
	(ROUND_UP_ALIGNED((x) * sizeof(type)) /	sizeof(type))

#define OFFSET_TOO_ALIGNED(x) \
	(((x) & 0xfff) == 0)

#define UNALIGN_OFFSET(x) \
	((x) + OFFSET_TOO_ALIGNED(x) ? ALIGNMENT : 0)


#define POINTERS_TOO_ALIGNED(x, y) \
	OFFSET_TOO_ALIGNED((((uintptr_t)x) ^ ((uintptr_t)y)))

#define ARRAY_LEN(x) sizeof(x) / sizeof((x)[0])

#define ROTATE(x, k) (((x) << (k)) | ((x) >> (sizeof(x) * 8 - (k))))

#define streq(a,b) (strcmp((a),(b)) == 0)

#define QUOTE_(x) #x
#define QUOTE(x) QUOTE_(x)

#define VERBOSE_DEBUG 0

#define STDERR_DEBUG(format, ...) do {                                 \
		fprintf (stderr, (format),## __VA_ARGS__);	       \
		fputc('\n', stderr);				       \
		fflush(stderr);					       \
	} while (0)

#define DEBUG STDERR_DEBUG
#define WARN STDERR_DEBUG
#if VERBOSE_DEBUG
#define MAYBE_DEBUG(args...) STDERR_DEBUG(args)
#else
#define MAYBE_DEBUG(args...) /*args */
#endif

#define DEBUG_LINENO() do {                                             \
		DEBUG("%s:%d:0: note: in %s, line %d\n",		\
		      __FILE__, __LINE__, __func__, __LINE__);		\
		fflush(stderr);}					\
	while(0)


#define FATAL_ERROR(args...) do {               \
		STDERR_DEBUG(args);		\
		exit(1);			\
	}while(0)


#define UNUSED __attribute__ ((unused))


#ifndef MAX
#define MAX(a, b) ( ( (a) >= (b) ) ? (a) : (b)  )
#endif
#ifndef MIN
#define MIN(a, b) ( ( (a) < (b) ) ? (a) : (b)  )
#endif


#endif /* _GOT_BITS_H */
