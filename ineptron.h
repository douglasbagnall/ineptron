/* Copyright (C) 2018 Douglas Bagnall <douglas@halo.gen.nz> */
#ifndef _GOT_INEPTRON_H
#define _GOT_INEPTRON_H 1

#include <stdint.h>
#include <malloc.h>
#include <time.h>
#include <stddef.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "bits.h"
/** Random numbers **/
#include "rng/rng.h"

static inline __attribute__((__malloc__, __assume_aligned__(ALIGNMENT),
			     __returns_nonnull__)) void *
malloc_aligned_or_die(size_t size){
	void *mem;
	if (sizeof(void*) * 2 >= ALIGNMENT){
		mem = malloc(size);
		if (mem == NULL){
			fputs("malloc failed!\n", stderr);
			abort();
		}
	} else {
		int err = posix_memalign(&mem, ALIGNMENT, size);
		if (err){
			fputs("posix_memalign failed!\n", stderr);
			fflush(stderr);
			abort();
		}
	}
	return mem;
}

static inline __attribute__((__malloc__,
			     __assume_aligned__(ALIGNMENT),
			     __returns_nonnull__)) void *
zalloc_aligned_or_die(size_t size){
	void *mem;
	if (sizeof(void*) * 2 >= ALIGNMENT) {
		mem = calloc(size, 1);
		if (mem == NULL){
			fputs("calloc failed!\n", stderr);
			abort();
		}
	} else {
		mem = malloc_aligned_or_die(size);
		memset(mem, 0, size);
	}
	return mem;
}


struct inept_interlayer {
	uint64_t *input;
	size_t input_width;
	uint64_t *yes;
	uint64_t *no;
	uint64_t *output;
	size_t output_width;
	uint64_t *weights;
};

struct inept_network {
	uint64_t *input;
	size_t n_input_bits;
	uint64_t *output;
	size_t n_output_bits;
	struct inept_interlayer *interlayers;
	uint depth;
	void *mem;
	size_t total_alloc_size;
	struct rng rng;
	uint output_resolution;
};

struct inept_il_trainer {
	struct inept_interlayer *il;
	uint64_t *inputs;
	uint64_t *error;
	uint64_t *delta;
	uint64_t *history;
	struct inept_ensemble_trainer *ctx;
};

struct inept_net_trainer {
	struct inept_network *net;
	struct inept_il_trainer *il_trainers;
	uint64_t *output_error;
	struct inept_ensemble_trainer *ctx;
};

struct inept_ensemble_trainer {
	struct inept_net_trainer *trainers;
	uint n_trainers;
	uint bptt_depth;
	uint bptt_offset;
	double train_p;
	struct rng rng;
	uint sparsity;
};


struct epoch_stats {
	uint count;
	double accuracy;
	double error;
};


/* functions */

/* ineptron.c */
struct inept_network *inept_new_network(uint *layer_sizes,
					uint n_layers,
					uint output_resolution);

void inept_delete_network(struct inept_network *net);


struct inept_ensemble_trainer * inept_new_trainer(uint *layer_sizes,
						  uint n_layers,
						  uint output_resolution,
						  uint n_nets);


void inept_delete_trainer(struct inept_ensemble_trainer *t);


void inept_ensemble_train(struct inept_ensemble_trainer *nt);




void inept_randomise_weights(struct inept_network *net, uint sparsity);
void inept_ensemble_trainer_randomise(struct inept_ensemble_trainer *t, uint sparsity);


uint64_t *inept_opinion(struct inept_network *net, uint64_t *inputs);


int inept_save_net(struct inept_network *net, char *filename);
struct inept_network *inept_load_net(char *filename);



int
inept_train_epochs(struct inept_ensemble_trainer *et,
		   uint64_t *inputs,
		   uint64_t *targets,
		   uint n_examples,
		   uint n_epochs,
		   uint sparsity,
		   int (*report_callback)(uint epoch, struct epoch_stats),
		   uint report_period
	);


#endif
