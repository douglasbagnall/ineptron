#include <stdbool.h>
#include "ineptron.h"
#include "rng/rng.h"

static size_t
calculate_net_sizes(struct inept_network *net,
		    const uint * const layer_sizes,
		    uint n_layers,
		    uint output_resolution)
{
	/* if net->mem is NULL, we only calculate the correct sizes. Otherwise
	   we fill out the various pointers. So what you do is call this
	   *twice*: first to establish the size (then allocate that size),
	   second to work out the pointers. */
	uint i;
	uintptr_t m = (uintptr_t)net->mem;
	uint byte_sizes[n_layers];

	byte_sizes[0] = ROUND_UP_ALIGNED_BITS(layer_sizes[0]);
	for (i = 1; i < n_layers - 1; i++) {
		byte_sizes[i] = ROUND_UP_ALIGNED_BITS(layer_sizes[i]);
	}
	byte_sizes[i] = ROUND_UP_ALIGNED_BITS(layer_sizes[i] *
					      output_resolution);

#define SET_VAR(x, y) do {			\
		if (net->mem != NULL) {		\
			x = (y);		\
		}				\
	} while(0)

#define SET_VAR_ARRAY(x, size) do {			\
		SET_VAR(x, (void *) m);			\
		m += size;				\
		if (OFFSET_TOO_ALIGNED(size)) {		\
			m += ALIGNMENT;			\
		}					\
	} while(0)

	for (i = 0; i < n_layers - 1; i++) {
		struct inept_interlayer *il = &net->interlayers[i];
		uint b0 = byte_sizes[i];
		uint b1 = byte_sizes[i + 1];

		uint lower = b0 + b1;
		uint upper = b1 * 2;

		SET_VAR_ARRAY(il->input, lower);
		SET_VAR(il->input_width, b0 / sizeof(uint64_t));
		SET_VAR_ARRAY(il->weights, lower * upper * 2);
		SET_VAR_ARRAY(il->yes, upper);
		SET_VAR_ARRAY(il->output, b1);
		SET_VAR(il->output_width, b1 / sizeof(uint64_t));
	}

#undef SET_VAR_ARRAY
#undef SET_VAR

	return (void *)m - net->mem;
}


struct inept_network *
inept_new_network(uint *layer_sizes,
		  uint n_layers,
		  uint output_resolution)
{
	if (n_layers < 2) {
		DEBUG("a net needs at least 2 layers\n");
		return NULL;
	}

	struct inept_network *net = calloc(1, sizeof(struct inept_network));
	if (net == NULL) {
		return NULL;
	}
	/* output_resolution is how many duplicate ouputs we have */
	if (output_resolution < 1) {
		output_resolution = 1;
	}
	layer_sizes[n_layers - 1] *= output_resolution;

	net->interlayers = calloc(n_layers, sizeof(struct inept_interlayer));
	if (net->interlayers == NULL) {
		free(net);
		return NULL;
	}

	size_t total_alloc = calculate_net_sizes(net,
						 layer_sizes,
						 n_layers,
						 output_resolution);

	net->mem = zalloc_aligned_or_die(total_alloc);

	size_t alloc2 = calculate_net_sizes(net,
					    layer_sizes,
					    n_layers,
					    output_resolution);

	DEBUG("calculated %zu first time, %zu second for alloc size\n",
	      total_alloc, alloc2);

	net->input = net->interlayers[0].input;
	net->n_input_bits = layer_sizes[0]; /* bit size, not rounded up! */

	net->depth = n_layers - 1;
	net->output = net->interlayers[net->depth].output;
	net->n_output_bits = layer_sizes[n_layers - 1];
	net->output_resolution = output_resolution;
	net->total_alloc_size = total_alloc;
	rng_init_maybe_randomly(&net->rng, -1ULL);
	return net;
}


void
inept_delete_network(struct inept_network *net)
{
	DEBUG("about to free network %p\n", net);
	free(net->mem);
	free(net->interlayers);
	free(net);
}

struct inept_ensemble_trainer *
inept_new_trainer(uint *layer_sizes,
		  uint n_layers,
		  uint output_resolution,
		  uint n_nets)
{
	uint i, j;
	struct inept_ensemble_trainer *t = calloc(1, sizeof(*t));
	t->trainers = calloc(n_nets, sizeof(t->trainers[0]));
	for (i = 0; i < n_nets; i++) {
		struct inept_net_trainer *nt = &t->trainers[i];
		nt->net = inept_new_network(layer_sizes, n_layers,
					    output_resolution);
		nt->ctx = t;
		nt->il_trainers = calloc(n_layers, sizeof(nt->il_trainers[0]));
		for (j = 0; j < n_layers; j++) {
			struct inept_interlayer *il = &nt->net->interlayers[j];
			struct inept_il_trainer *il_t = &nt->il_trainers[j];
			nt->il_trainers[j].il = il;
			/* XXX make these unaligned */
			il_t->error = calloc(il->output_width, sizeof(uint64_t));
			il_t->delta = calloc(il->output_width * il->input_width,
					     sizeof(uint64_t));

			il_t->ctx = t;
		}
		nt->output_error = nt->il_trainers[j].error;
	}
	return t;
}


void
inept_delete_trainer(struct inept_ensemble_trainer *t)
{
	/* XXX todo */
}


static inline void
calculate_interlayer(struct inept_interlayer *il,
		     struct rng *rng)
{
	int x, y, i;
	/* output size = yes + nos */
	/* input size = inputs + recurrent */
	size_t output_size = il->output_width * 2;
	size_t input_size = il->input_width + il->output_width;
	uint64_t *weights = il->weights;
	uint64_t *in = il->input;
	uint64_t *result = il->output;
	uint64_t *yes = il->yes;
	uint64_t *outputs = yes;
	uint64_t *no = il->no;
	ASSUME_ALIGNED(weights);
	ASSUME_ALIGNED(in);
	ASSUME_ALIGNED(yes);
	ASSUME_ALIGNED(outputs);
	ASSUME_ALIGNED(no);
	ASSUME_ALIGNED(result);
	ASSUME_ALIGNED_SIZE(il->output_width, uint64_t);
	ASSUME_ALIGNED_SIZE(output_size, uint64_t);
	ASSUME_ALIGNED_SIZE(input_size, uint64_t);
	/* copy in the previous outputs */
	memcpy(in + il->input_width,
	       yes,
	       il->output_width * sizeof(uint64_t));
	memset(yes, 0, output_size * sizeof(uint64_t));
	uint64_t *w_row = weights;
	for (y = 0; y < input_size; y++) {
		uint64_t inputs = in[y];
		for (i = 0;  i < sizeof(uint64_t); i++) {
			if (inputs & 1) {
				ASSUME_ALIGNED(w_row);
				for (x = 0; x < output_size; x++) {
					outputs[x] |= w_row[x];
				}
			}
			inputs >>= 1;
			w_row += output_size;
		}
	}

	for (x = 0; x < il->output_width; x++) {
		uint64_t on = yes[x] & ~no[x];
		uint64_t off = no[x] & ~yes[x];
		result[x] |= on;
		result[x] &= ~off;
	}
}


static inline void
backprop_interlayer(struct inept_interlayer *il,
		    struct rng *rng,
		    uint sparsity)
{
	int x, y, i;
	/* output size = yes + nos */
	/* input size = inputs + recurrent */
	size_t output_size = il->output_width * 2;
	size_t input_size = il->input_width + il->output_width;
	uint64_t *weights = il->weights;
	uint64_t *in = il->input;
	uint64_t *result = il->output;
	uint64_t *yes = il->yes;
	uint64_t *no = il->no;
	ASSUME_ALIGNED(weights);
	ASSUME_ALIGNED(in);
	ASSUME_ALIGNED(yes);
	ASSUME_ALIGNED(no);
	ASSUME_ALIGNED(result);
	ASSUME_ALIGNED_SIZE(il->output_width, uint64_t);
	ASSUME_ALIGNED_SIZE(output_size, uint64_t);
	ASSUME_ALIGNED_SIZE(input_size, uint64_t);

	/* calculate the wrongness of the output */

	for (x = 0; x < il->output_width; x++) {
		uint64_t on = yes[x] & ~no[x];
		uint64_t off = no[x] & ~yes[x];
		result[x] |= on;
		result[x] &= ~off;
	}


	uint64_t *w_row = weights;
	for (y = 0; y < input_size; y++) {
		uint64_t inputs = in[y] & sparse_rand64(rng, sparsity);
		for (i = 0;  i < sizeof(uint64_t); i++) {
			if (inputs & 1) {
				ASSUME_ALIGNED(w_row);
				for (x = 0; x < output_size; x++) {
					yes[x] |= w_row[x];
				}
			}
			inputs >>= 1;
			w_row += output_size;
		}
	}

	for (x = 0; x < il->output_width; x++) {
		uint64_t on = yes[x] & ~no[x];
		uint64_t off = no[x] & ~yes[x];
		result[x] |= on;
		result[x] &= ~off;
	}
}


void
inept_randomise_weights(struct inept_network *net, uint sparsity)
{
	uint i, j;
	for (i = 0; i < net->depth; i++) {
		struct inept_interlayer *il = &net->interlayers[i];
		uint size = ((il->output_width + il->input_width) *
			     il->output_width * 2);
		for (j = 0; j < size; j++) {
			il->weights[j] = sparse_rand64(&net->rng, sparsity);
		}
	}
}

void
inept_ensemble_trainer_randomise(struct inept_ensemble_trainer *t, uint sparsity)
{
	uint i;
	for (i = 0;  i < t->n_trainers; i++) {
		inept_randomise_weights(t->trainers[i].net, sparsity);
	}
}


/* inept_opinion returns an output vector given some inputs if input
   pointer points to existing net inputs (from previous opinion,
   training or explicit setting), time is saved.
   //XXX not checking for overlap (don't do that)
*/
uint64_t *
inept_opinion(struct inept_network *net, uint64_t *inputs)
{
	uint i;
	if (inputs != NULL) {
		uint n_inputs = net->n_input_bits / (8 * sizeof(uint64_t));
		/* we *could* just switch the pointers */
		for (i = 0; i < n_inputs; i++) {
			net->input[i] = inputs[i];
		}
	}

	for (i = 0; i < net->depth; i++) {
		calculate_interlayer(&net->interlayers[i], &net->rng);
	}
	return net->output;
}


static inline void
modify_interlayer(struct inept_interlayer *il,
		  struct rng *rng,
		  uint64_t *error,
		  uint sparsity)
{
	/* backprop first */
	uint x, y, i;
	uint64_t *in = il->input;
	uint64_t *out = il->output;
	size_t output_size = il->output_width * 2;
	size_t input_size = il->input_width + il->output_width;
	uint64_t *weights = il->weights;
	ASSUME_ALIGNED(weights);
	ASSUME_ALIGNED(in);
	ASSUME_ALIGNED_SIZE(output_size, uint64_t);
	ASSUME_ALIGNED_SIZE(input_size, uint64_t);

	uint64_t *w_row = weights;

	ASSUME_ALIGNED(in);
	ASSUME_ALIGNED(out);
	for (y = 0; y < il->input_width; y++) {
		uint64_t inputs = in[y];
		for (i = 0;  i < sizeof(uint64_t); i++) {
			if (inputs & 1) {
				ASSUME_ALIGNED(w_row);
				for (x = 0; x < output_size; x++) {
					uint64_t e = sparsify(rng, error[x],
							      sparsity);
					w_row[x] ^= e;
				}
			}
			inputs >>= 1;
			w_row += output_size;
		}
	}
}

void
inept_modify_weights(struct inept_network *net,
		     uint64_t *correct,
		     uint64_t *error,
		     uint sparsity)
{
	int i, j;
	/* error should have enough room for the largest layer */

	for (i = net->depth - 1; i >= 0; i--) {
		struct inept_interlayer *il = & net->interlayers[i];
		uint64_t *yes_error = error;
		uint64_t *no_error = yes_error + il->output_width;
		for (j = 0; j < il->output_width; j++) {
			yes_error[j] = il->output[j] & ~correct[j];
			no_error[j] = ~il->output[j] & correct[j];
		}
		modify_interlayer(il,
				  &net->rng,
				  error,
				  sparsity);

	}
}


static void
train_interlayer(struct inept_il_trainer *il_trainer,
		 struct rng *rng,
		 uint64_t *targets,
		 uint sparsity)
{
	uint k;
	struct inept_interlayer *il = il_trainer->il;
	size_t o_size = il->output_width;
	uint64_t *outputs = il->output;
	uint64_t *yes = il->yes;
	uint64_t *no = il->no;
	uint64_t *yes_error = il_trainer->error;
	uint64_t *no_error = yes_error + o_size;
	ASSUME_ALIGNED(outputs);
	ASSUME_ALIGNED(targets);
	ASSUME_ALIGNED(yes_error);
	ASSUME_ALIGNED(no_error);
	ASSUME_ALIGNED(yes);
	ASSUME_ALIGNED(no);

	for (k = 0; k < o_size; k++) {
		uint64_t got = outputs[k];
		uint64_t wanted = targets[k];
		uint64_t wrong = got ^ wanted;

		/* there are other things we could do here, for the cases
		   resulting in no change, where the yes or no would be wrong
		   if not for the counterbalancing result. e.g.: correct is 1,
		   yes is 0 no is 0, previous is 1. the correct answer is
		   given, but perhaps yes still should have been 1.
		*/
		//uint64_t changed = yes[k] ^ no[k];
		uint64_t wrong_1 = wrong & got;
		uint64_t wrong_0 = wrong & ~got;
		uint64_t yes_wrong = wrong_0 & yes[k];
		uint64_t no_wrong = wrong_1 & no[k];
		yes_error[k] = yes_wrong;
		no_error[k] = no_wrong;
	}
	modify_interlayer(il,
			  rng,
			  yes_error,
			  sparsity);
}

static void
train_net(struct inept_net_trainer *nt,
	  uint64_t *inputs,
	  uint64_t *targets,
	  uint sparsity)
{
	int i;
	struct inept_network *net = nt->net;

	/* advance bptt offset */
	uint bptt_offset = nt->ctx->bptt_offset + 1;
	if (bptt_offset == nt->ctx->bptt_depth) {
		bptt_offset = 0;
	}
	nt->ctx->bptt_offset = bptt_offset;

	/* Advance bptt, so we save inputs for all layers */
	for (i = net->depth - 1; i >= 0; i--) {
		struct inept_il_trainer *ilt = &nt->il_trainers[i];
		struct inept_interlayer *il = &net->interlayers[i];
		size_t w = il->input_width + il->output_width;
		uint64_t *layer = ilt->history + w * bptt_offset;
		il->input = layer;
	}

	inept_opinion(net, inputs);

	for (i = net->depth - 1; i >= 0; i--) {
		train_interlayer(&nt->il_trainers[i],
				 &nt->net->rng,
				 targets,
				 sparsity);
	}
}

int
inept_train(struct inept_ensemble_trainer *et,
	    uint64_t *inputs,
	    uint64_t *targets,
	    size_t n,
	    uint sparsity,
	    int (*report_callback)(uint epoch, struct epoch_stats),
	    uint report_period,
	    uint epoch)

{
	uint i, j;
	struct epoch_stats stats = {0};
	const struct epoch_stats zero_stats = {0};
	struct inept_network *net_0 = et->trainers[0].net;
	struct rng *rng = &net_0->rng;
	uint output_size = ROUND_UP_ALIGNED_BITS(net_0->n_output_bits);
	uint input_size = ROUND_UP_ALIGNED_BITS(net_0->n_input_bits);
	uint report_countdown = report_period;

	for (i = 0; i < n; i++) {
		bool training = (random_01(rng) <= et->train_p);

		for (j = 0; j < et->n_trainers; j++) {
			struct inept_net_trainer *nt = &et->trainers[j];
			if (training) {
				train_net(nt, inputs, targets, sparsity);
			} else {
				/* just advance */
			}
		}
		inputs += input_size;
		targets += output_size;

		if (--report_countdown == 0) {
			int ret = report_callback(epoch, stats);
			if (ret) {
				return ret;
			}
			stats = zero_stats;
			report_countdown = report_period;
		}
	}
	return 0;
}


int
inept_train_epochs(struct inept_ensemble_trainer *et,
		   uint64_t *inputs,
		   uint64_t *targets,
		   uint n_examples,
		   uint n_epochs,
		   uint sparsity,
		   int (*report_callback)(uint epoch, struct epoch_stats),
		   uint report_period)
{
	uint epoch;
	for (epoch = 1; epoch <= n_epochs; epoch++) {
		inept_train(et,
			    inputs,
			    targets,
			    n_examples,
			    et->sparsity,
			    report_callback,
			    report_period,
			    epoch);
	}
	return 0;
}
