#include <stdint.h>

#include "ineptron.h"

#define CHAR_BITS 5

int numchars = 0; // oh god how do i return two things ahhhh why is this not rust, i'll just use a global variable for the size -_-

uint64_t *load_file(FILE *fp) {
  fseek(fp, 0L, SEEK_END);
  uint32_t filesize = ftell(fp);

  numchars = filesize;
  uint64_t *ret = malloc_aligned_or_die((filesize * CHAR_BITS + 1) / 8);

  // Assumes ascii.

  rewind(fp);

  char c;
  char newc;
  uint64_t i = 0;
  while ((c = getc(fp)) != EOF) {
    if (c >= 0x41 && c <= 0x5A) {
      // capital
      newc = c-0x41;
    } else if (c >= 0x61 && c <= 0x7A) {
      // lower case
      newc = c-0x61;
    } else if (c == ' '){
      newc = 0x1A;
    } else if (c == '\n'){
      newc = 0x1B;
    } else if (c == ','){
      newc = 0x1C;
    } else if (c == '.'){
      newc = 0x1D;
    } else if (c == '!'){
      newc = 0x1E;
    } else { // ?
      newc = 0x1F;
    }

    /* putc(newc, stdout); */

    for (int bit = 0; bit < CHAR_BITS; bit++) {
      uint64_t idx    = (i*CHAR_BITS + bit)/64;
      uint64_t offset = (i*CHAR_BITS + bit)%64;

      ret[idx] = ret[idx] | (((newc >> ((CHAR_BITS-bit) - 1))) & 1) << (64 - offset - 1);
    }

    i++;
  }

  return ret;
}

uint64_t *load_file_from_path(char *path) {
  FILE *fp = fopen(path, "r");
  if (fp == NULL) {
    fprintf(stderr, "load file '%s' failed!\n", path);
    abort();
  }
  uint64_t *data = load_file(fp);
  fclose(fp);
  return data;
}
