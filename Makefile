all:

BASENAME = ineptron
PYNAME = $(BASENAME).so

DEBUG_FLAGS = -O3 -g

COMMON_OPTS =   -pipe -ffast-math -std=gnu11 -Wall -D_GNU_SOURCE  -flax-vector-conversions

PY_VERSION = $(shell python -c 'import sys; print sys.version[:3]')
PYTHON = python$(PY_VERSION)

PY_CFLAGS =   $(DEBUG_FLAGS)  -Wall -pthread -fno-strict-aliasing -DNDEBUG -Wstrict-prototypes -fPIC -I/usr/include/$(PYTHON)


ALL_CFLAGS	= $(DEBUG_FLAGS) $(COMMON_OPTS) $(LOCAL_OPTS) $(CFLAGS) -fPIC

.c.o:
	$(CC) -c $(ALL_CFLAGS) $(CPPFLAGS) -o $@ $<

.c.s:
	$(CC) -S -fverbose-asm  $(ALL_CFLAGS) $(CPPFLAGS) -o $@ $<

%.i:	%.c
	$(CC)  -E  $(ALL_CFLAGS) $(CPPFLAGS) -o $@ $<

all:   ineptron.o

clean:
	rm -f *.o *.a *~ *.so

image-clean:
	rm -f images/*.pbm

#ineptronmodule.o: ineptronmodule.c
#	$(CC) -c $(PY_CFLAGS) $(ALL_CFLAGS) $(CPPFLAGS) -o $@ $<

ineptron.so: ineptron.c ineptron-numpy.c colour.c
	#$(CC) -pthread -shared -Wl,-O1 $^ -lrt -o $@
	python setup.py build_ext --inplace

python: ineptron.so

python-test: python
	python python-test.py

.PHONY: all clean python python-test

TAGS:
	etags $$(find -name '*.[ch]' |grep -v -F '/junk')
