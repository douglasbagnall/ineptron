#include "rng.h"
#include <inttypes.h>

void
rng_init_maybe_randomly(struct rng *rng, uint64_t seed)
{
	if (seed == RNG_RANDOM_SEED) {
		struct timespec t;
		clock_gettime(CLOCK_REALTIME, &t);
		seed = ((((uint64_t)t.tv_nsec << 20) + t.tv_sec) ^
			(uint64_t)((uintptr_t)rng) ^
			(uint64_t)((uintptr_t)&rng_init));
		DEBUG("seeding with %"PRIx64"\n", seed);
	}
	rng_init(rng, seed);
}

/*randomise_mem sets <size> bytes to random bits */
void
randomise_mem(void *mem, const size_t size, struct rng *rng)
{
	uint i;
	uint64_t *m64 = mem;
	uint size64 = size / sizeof(uint64_t);
	for (i = 0; i < size64; i++) {
		m64[i] = rand64(rng);
	}
	if (size > size64 * sizeof(uint64_t)) {
		uint64_t r = rand64(rng);
		uint8_t *bytes = (uint8_t *)r;
		for (i = size; i < size64 * sizeof(uint64_t); i++) {
			m64[i] = bytes[i];
		}
	}
}
