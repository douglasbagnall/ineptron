/* Copyright 2014 Douglas Bagnall <douglas@halo.gen.nz> LGPL */
#ifndef _GOT_RNG_H
#define _GOT_RNG_H 1
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include "sfc.h"

#define RNG_RANDOM_SEED -1ULL

void rng_init_maybe_randomly(struct rng *rng, uint64_t seed);
void randomise_mem(void *mem, const size_t size, struct rng *rng);


static inline uint64_t
sparse_rand64(struct rng *rng, uint sparsity)
{
	uint i;
	if (sparsity == 0) {
		return UINT64_MAX;
	}
	uint64_t b = rand64(rng);
	for (i = 1; i < sparsity; i++) {
		b &= rand64(rng);
		if (b == 0) {
			return 0;
		}
	}
	return b;
}

static inline uint64_t
sparsify(struct rng *rng, uint64_t x, uint sparsity)
{
	while (sparsity != 0 && x != 0) {
		x &= rand64(rng);
		sparsity--;
	}
	return x;
}

#define RANDOM_01_SCALE (1.0 / UINT64_MAX)

static inline double
random_01(struct rng *rng)
{
	return rand64(rng) * RANDOM_01_SCALE;
}

#endif
