#ifndef _GOT_LCG_MIX_H
#define _GOT_LCG_MIX_H 1
/* 
*/

#include "../bits.h"

struct rng
{
	uint64_t a, b;
};

const uint64_t LCG_MUL = 2862933555777941757;
const uint64_t LCG_ADD = 3037000493;

static inline uint64_t
rand64(struct rng *rng)
{
	uint64_t a = rng->a;
	uint64_t b = rng->b;
	b += ROTATE(a, 13);
	a *= LCG_MUL;
	a += LCG_ADD;
	rng->a = a;
	rng->b = b;       
	return a ^ b;
}

static inline void
rng_init(struct rng *rng, uint64_t seed)
{
	int i;
	rng->a = seed + LCG_ADD;
	rng->b = seed ^ LCG_MUL;
	for (i = 0; i < 12; i++) {
		rand64(rng);
	}
}
#endif
