/* Copyright 2017 Catalyst IT Ltd.  LGPL.
 *
 * Written by Douglas Bagnall <douglas@halo.gen.nz>
 *
 * Read in numpy arrays for input, output, and target values.
 * Based on recur/py-recur-numpy.c
*/
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "py-helpers.h"
#include <numpy/arrayobject.h>
#include "badmaths.h"
#include <fenv.h>
#include <stdbool.h>
#include "ineptron.h"
#include "rng/sfc.h"
#include "rng/rng.h"
#include "colour.h"

#define DEFAULT_SAMPLE_ROUNDS 32

typedef struct {
	PyObject_HEAD
	uint *seen_counts;
	uint output_resolution;
	struct inept_ensemble_trainer *trainer;
	uint seen_sum;
	uint used_sum;
} Trainer;

static PyObject *PyExc_IneptError;

static void
Trainer_dealloc(Trainer *self)
{
	if (self->trainer) {
		inept_delete_trainer(self->trainer);
		self->trainer = NULL;
	}
	self->ob_type->tp_free((PyObject *) self);
}

static PyObject *
Trainer_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Trainer *self = (Trainer *)PyType_GenericNew(type, args, kwds);
    self->output_resolution = 1;
    return (PyObject *)self;
}

/* get a 2d array from numpy */

static PyArrayObject *
get_2d_array_check_size(PyObject *arg,
			uint w,
			uint h,
			const char *name,
			int type)
{
	PyArrayObject *array = NULL;
	npy_intp *shape = NULL;
	array = (PyArrayObject *) PyArray_FROM_OTF(arg, type,
						   NPY_ARRAY_IN_ARRAY);
	if (array == NULL) {
		return NULL;
	}
	if (PyArray_NDIM(array) != 2) {
		PyErr_Format(PyExc_ValueError,
			     "%s array should be 2 dimension, not %d",
			     name, PyArray_NDIM(array));
		goto error;
	}
	shape = PyArray_SHAPE(array);

	/* zero h, w means we don't care/know about that dimension */
	if (h != 0 && shape[0] != h) {
		PyErr_Format(PyExc_ValueError,
			     "%s array has %ld rows, expected %u",
			     name, shape[0], h);
		goto error;
	}
	if (w != 0 && shape[1] != w) {
		PyErr_Format(PyExc_ValueError,
			     "%s array has %ld columns, expected %u",
			     name, shape[1], w);
		goto error;
	}

	return array;
 error:
	Py_DECREF(array);
	return NULL;
}

struct uint_array {
	uint *array;
	size_t length;
};

static int get_uint_array(PyObject *src, struct uint_array *dest)
{
	if (! PySequence_Check(src)) {
		goto error;
	}
	size_t i;
	dest->length = PySequence_Length(src);
	dest->array = calloc(dest->length, sizeof(uint));

	for (i = 0; i < dest->length; i++) {
		PyObject *o = PySequence_ITEM(src, i);
		if (! PyLong_Check(o)) {
			goto error;
		}
		long v = PyLong_AsLong(o);
		if (v < 0 || v > UINT_MAX) {
			goto error;
		}
		dest->array[i] = (uint)v;
	}
	return 1;

  error:
	PyErr_SetString(PyExc_ValueError,
			"layer_sizes should be an array "
			"of positive integers");
	return 0;
}

static int
Trainer_init(Trainer *self, PyObject *args, PyObject *kwds)
{
	/* mandatory arguments */
	struct uint_array layer_sizes;
	/* optional arguments */
	unsigned long long rng_seed = 2;
	const char *log_file = NULL;
	const char *filename = NULL;
	const char *basename = NULL;
	int bptt_depth = 30;
	int verbose = 0;
	uint output_resolution = 1;
	uint n_nets = 12;
	uint sparsity = 1;

	static char *kwlist[] = { "layer_sizes",            /* O& */
				  "log_file",               /* z  */
				  "filename",               /* z  */
				  "bptt_depth",             /* I  */
				  "rng_seed",               /* K  */
				  "basename",               /* z  */
				  "verbose",                /* i  */
				  "output_resolution",      /* I  */
				  "n_nets",                 /* I  */
				  "sparsity",               /* I */
		NULL
	};

	if (!PyArg_ParseTupleAndKeywords(args, kwds,
					 "O&|zziKzi",
					 kwlist,
					 get_uint_array,  /* O& */
					 &layer_sizes,
					 &log_file,        /* z  */
					 &filename,        /* z  */
					 &bptt_depth,      /* I  */
					 &rng_seed,        /* K  */
					 &basename,        /* z  */
					 &verbose,         /* i  */
					 output_resolution, /* I */
					 n_nets,            /* I */
					 sparsity          /* I */
	    )) {
		return -1;
	}

	self->trainer = inept_new_trainer(layer_sizes.array,
					  layer_sizes.length,
					  output_resolution,
					  n_nets);

	inept_ensemble_trainer_randomise(self->trainer, sparsity);
	return 0;
}

static struct inept_network *
get_first_trainer_net(Trainer *self)
{
	struct inept_ensemble_trainer *t = self->trainer;
	if (t == NULL ||
	    t->n_trainers == 0 ||
	    t->trainers == NULL ||
	    t->trainers[0].net == NULL) {
		PyErr_Format(PyExc_IneptError, "Trainer has no nets");
		return NULL;
	}
	return t->trainers[0].net;
}


static UNUSED PyObject *
get_inept_network_size_t(Trainer *self, void *_offset)
{
	uintptr_t offset = (uintptr_t)_offset;
	struct inept_network *net = get_first_trainer_net(self);
	if (net == NULL) {
		return NULL;
	}

	void *addr = ((void *)net) + offset;
	size_t x = *(size_t *)addr;
	return PyLong_FromSize_t(x);
}

static UNUSED PyObject *
get_inept_network_uint(Trainer *self, void *_offset)
{
	uintptr_t offset = (uintptr_t)_offset;
	struct inept_network *net = get_first_trainer_net(self);
	if (net == NULL) {
		return NULL;
	}

	void *addr = ((void *)net) + offset;
	uint x = *(uint *)addr;
	return PyLong_FromUnsignedLong((unsigned long)x);
}

static UNUSED PyObject *
get_inept_ensemble_trainer_double01(Trainer *self, void *_offset)
{
	struct inept_ensemble_trainer *t = self->trainer;
	if (t == NULL) {
		PyErr_Format(PyExc_IneptError, "Trainer has no nets");
		return NULL;
	}
	uintptr_t offset = (uintptr_t)_offset;
	void *addr = ((void *)t) + offset;
	double x = *(double *)addr;
	return PyFloat_FromDouble(x);
}

static UNUSED int
set_inept_ensemble_trainer_double01(Trainer *self, PyObject *value,
				    void *_offset)
{
	struct inept_ensemble_trainer *t = self->trainer;
	uintptr_t offset = (uintptr_t)_offset;
	void *addr = ((void *)t) + offset;
	PyObject *fvalue;
	double v;
	if (t == NULL) {
		PyErr_Format(PyExc_IneptError, "Trainer is uninitialised");
		return -1;
	}
	fvalue = PyNumber_Float(value);
	if (fvalue == NULL) {
		goto bad_value;
	}
	v = PyFloat_AS_DOUBLE(fvalue);
	Py_DECREF(fvalue);
	if (v < 0.0 || v > 1.0) {
		goto bad_value;
	}
	*(double *)addr = v;
	return 0;
  bad_value:
	PyErr_Format(PyExc_ValueError, "A number between 0 and 1 is required");
	return -1;
}


static UNUSED PyObject *
get_inept_ensemble_trainer_uint(Trainer *self, void *_offset)
{
	struct inept_ensemble_trainer *t = self->trainer;
	if (t == NULL) {
		PyErr_Format(PyExc_IneptError, "Trainer has no nets");
		return NULL;
	}
	uintptr_t offset = (uintptr_t)_offset;
	void *addr = ((void *)t) + offset;
	uint x = *(uint *)addr;
	return PyLong_FromUnsignedLong((unsigned long)x);
}

static UNUSED int
set_inept_ensemble_trainer_uint(Trainer *self, PyObject *value,
				void *_offset)
{
	struct inept_ensemble_trainer *t = self->trainer;
	uintptr_t offset = (uintptr_t)_offset;
	void *addr = ((void *)t) + offset;
	uint v;
	if (t == NULL) {
		PyErr_Format(PyExc_IneptError, "Trainer is uninitialised");
		return -1;
	}
	v = (uint) PyLong_AsUnsignedLong(value);
	if (PyErr_Occurred()) {
		return -1;
	}
	*(uint *)addr = v;
	return 0;
}



#define TRAINER_GETSETTER(name, object, type, doc) {			\
			#name,						\
			(getter) get_ ## object ## _ ## type,		\
			(setter) set_ ## object ## _ ## type,		\
			doc,						\
			(void *)(offsetof(struct object, name))	\
		}

#define TRAINER_GETTER(name, object, type, doc) {			\
			#name,						\
			(getter) get_ ## object ## _ ## type,		\
			NULL,						\
			doc " (read only)",				\
			(void *)(offsetof(struct object, name))	\
		}


static PyGetSetDef Trainer_getsetters[] = {
	TRAINER_GETSETTER(train_p,
			  inept_ensemble_trainer,
			  double01,
			  "probability of training a node"
		),
	TRAINER_GETSETTER(sparsity,
			  inept_ensemble_trainer,
			  uint,
			  "how sparsely to make modifications "
			  "(fractional power of 2)"
		),
	TRAINER_GETTER(depth,
		       inept_network,
		       uint,
		       "network depth"
		),
	TRAINER_GETTER(n_input_bits,
		       inept_network,
		       size_t,
		       "number of input bits"
		),
	TRAINER_GETTER(n_output_bits,
		       inept_network,
		       size_t,
		       "number of output bits"
		),
	TRAINER_GETTER(output_resolution,
		       inept_network,
		       uint,
		       "how often the output nodes are duplicated"
		),
	TRAINER_GETTER(n_trainers,
		       inept_ensemble_trainer,
		       uint,
		       "trainer population size"
		),
	TRAINER_GETTER(bptt_depth,
		       inept_ensemble_trainer,
		       uint,
		       "how deep the bptt training is"
		),

	{NULL}
};


static inline void
pack_bool_repeat(bool *in, uint64_t *out, uint n, uint repeat)
{
	uint i;
	uint j = 0;
	uint k;
	uint64_t bit = 1;
	uint64_t o = 0;
	for (i = 0; i < n; i++) {
		bool b = in[i];
		for (k = 0; k < repeat; k++) {
			o |= b ? bit : 0;
			bit <<= 1;
			if (bit == 0) {
				out[j] = o;
				j++;
				o = 0;
				bit = 1;
			}
		}
	}
	if (o != 0) {
		out[j] = o;
	}
}



static inline void
pack_bool(bool *in, uint64_t *out, uint n)
{
	uint i = 0;
	uint j = 0;
	uint64_t bit = 1;
	uint64_t o = 0;
	for (i = 0; i < n; i++) {
		o |= in[i] ? bit : 0;
		bit <<= 1;
		if (bit == 0) {
			out[j] = o;
			j++;
			o = 0;
			bit = 1;
		}
	}
	if (o != 0) {
		out[j] = o;
	}
}


static inline void
pack_float(float *in, uint64_t *out, uint n, struct rng *rng, void *mem)
{
	uint i;
	bool *bools;
	if (mem == NULL) {
		bools = malloc_aligned_or_die(n * sizeof(bool));
	} else {
		bools = mem;
	}
	for (i = 0; i < n; i++) {
		bools[i] = (random_01(rng) < in[i]) ? 1 : 0;
	}
	pack_bool(bools, out, n);
	if (mem == NULL) {
		free(mem);
	}
}


int report_callback(uint epoch,
		    struct epoch_stats stats) {
	if (stats.count == 0) {
		DEBUG("epoch %u trained on zero examples!", epoch);
	} else {
		double acc = stats.accuracy / stats.count;
		double err = stats.error / stats.count;
		DEBUG("epoch %3u trained on %5u; alleged"
		      " accuracy %s%.2f" C_NORMAL
		      " error %s%.2f" C_NORMAL,
		      epoch,
		      stats.count,
		      colourise_float01(acc, true),
		      acc,
		      colourise_float01(1.0f - (err * err), false),
		      err);
	}
	if (PyErr_CheckSignals() == -1) {
		/* this will allow a control-C to interrupt. */
		DEBUG("interrupted");
		return 1;
	}
	return 0;
}


static PyObject *
Trainer_train(Trainer *self, PyObject *args, PyObject *kwds)
{
	uint i, j;
	PyObject *input_arg = NULL;
	PyArrayObject *inputs = NULL;
	PyObject *target_arg = NULL;
	PyArrayObject *targets = NULL;
	double temperature = 0;
	double balance = 0;
	npy_intp *input_shape;
	npy_intp *target_shape;
	uint n_epochs = 0;
	uint report_period = 1024;
	int input_type = NPY_BOOL;
	int output_type = NPY_BOOL;
	int sample_rounds = DEFAULT_SAMPLE_ROUNDS;
	struct inept_network *net = get_first_trainer_net(self);
	if (net == NULL) {
		PyErr_Format(PyExc_IneptError, "Not ready: we have no net");
		return NULL;
	}
	struct inept_ensemble_trainer *et = self->trainer;

	static char *kwlist[] = { "features",		/* O  */
				  "targets",		/* O  */
				  "n_epochs",		/* I  */
				  "temperature",	/* d  */
				  "balance",		/* d  */
				  "input_type",		/* i  */
				  "output_type",	/* i  */
				  "sample_rounds",	/* i */
				  NULL
	};

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "OOI|Oddiii", kwlist,
					 &input_arg,	/* O  */
					 &target_arg,	/* O  */
					 &n_epochs,	/* I  */
					 &temperature,	/* d  */
					 &balance,	/* d  */
					 &input_type,   /* i  */
					 &output_type,  /* i  */
					 &sample_rounds,/* i  */
					 NULL)) {
		return NULL;
	}

	if (output_type != NPY_BOOL) {
		PyErr_Format(PyExc_TypeError,
			     "unsupported output data type "
			     "(bool only, sorry)");
		return NULL;
	}
	if (input_type != NPY_BOOL &&
	    input_type != NPY_FLOAT32 &&
	    input_type != NPY_DOUBLE) {
		PyErr_Format(PyExc_TypeError,
			     "unsupported input data type "
			     "(expected bool or float)");
		return NULL;
	}

	inputs = get_2d_array_check_size(input_arg,
					 net->n_input_bits,
					 0,
					 "input",
					 input_type);
	if (inputs == NULL) {
		return NULL;
	}

	/* targets need same height as inputs */
	input_shape = PyArray_SHAPE(inputs);

	targets = get_2d_array_check_size(target_arg,
					  net->n_output_bits,
					  input_shape[0],
					  "target",
					  output_type);
	if (targets == NULL) {
		Py_DECREF(inputs);
		return NULL;
	}

	target_shape = PyArray_SHAPE(targets);


	/* We have our python data arrays, but they are not in the right
	   format (packed bits).

	   for inputs:

	   1. allocate the bit arrays of the right size (or bigger)
	   2. if bool, pack the bits
	   3. if float, sample and pack the bits (and sample many times)


	   for outputs:
	   1. allocate
	   2. if bool, pack bits
	   3. if float ?
              we should have multi-outputs, so threshold for each?
	*/

	size_t i_stride = net->interlayers[0].input_width; /* in uint64_t */
	size_t t_stride = net->interlayers[net->depth - 1].output_width;
	size_t n_examples = input_shape[0];
	if (input_type == NPY_BOOL) {
		sample_rounds = 1;
	}
	uint64_t *input_bits = zalloc_aligned_or_die(i_stride * n_examples * sample_rounds);
	uint64_t *target_bits = zalloc_aligned_or_die(t_stride *
						      n_examples *
						      sample_rounds *
						      self->output_resolution);
	uint epoch_len = n_examples;

	if (input_type == NPY_BOOL) {
		npy_intp index[2] = {0, 0};
		for (i = 0; i < n_examples; i++) {
			index[0] = i;
			pack_bool(PyArray_GetPtr(inputs, index),
				  input_bits + i * i_stride,
				  net->n_input_bits);
		}
	} else {
		/* NPY_FLOAT32 or NPY_DOUBLE */
		PyArrayObject *_inputs = inputs;
		if (input_type == NPY_DOUBLE) {
			PyArray_Descr *desc = PyArray_DescrFromType(NPY_FLOAT32);
			_inputs = (PyArrayObject *)PyArray_FromAny((PyObject *)inputs,
								   desc,
								   2, 2,
								   NPY_ARRAY_IN_ARRAY,
								   NULL);
		} else {
			Py_INCREF(_inputs);
		}
		uint64_t *in_row = input_bits;
		bool *bool_tmp = zalloc_aligned_or_die(input_shape[1]);
		for (j = 0; j < sample_rounds; j++) {
			npy_intp index[2] = {0, 0};
			for (i = 0; i < n_examples; i++) {
				index[0] = i;
				pack_float(PyArray_GetPtr(_inputs, index),
					   in_row,
					   net->n_input_bits,
					   &net->rng, bool_tmp);
				in_row += i_stride;
			}
		}
		free(bool_tmp);
		Py_DECREF(_inputs);
		epoch_len *= sample_rounds;
	}

	/* get the targets into the packed bits with self->output_resoution
	   bits per output */

	npy_intp index[2] = {0, 0};
	for (i = 0; i < n_examples; i++) {
		index[0] = i;
		pack_bool_repeat(PyArray_GetPtr(targets, index),
				 target_bits + i * t_stride,
				 net->n_output_bits,
				 self->output_resolution);
	}
	/* at this point we have nothing python specific? */

	inept_train_epochs(et,
			   input_bits,
			   target_bits,
			   epoch_len,
			   n_epochs,
			   et->sparsity,
			   report_callback,
			   report_period);

	Py_DECREF(inputs);
	Py_DECREF(targets);
	return Py_BuildValue("");
 error:
	if (inputs != NULL) {
		Py_DECREF(inputs);
	}
	if (targets != NULL) {
		Py_DECREF(targets);
	}
	return NULL;
}



static const char Trainer_doc[] =
    "Numpy interface to ineptron trainer\n\n";

static PyMethodDef Trainer_methods[] = {
	{"train", (PyCFunction) Trainer_train, METH_VARARGS | METH_KEYWORDS,
	 "train the net with inpuit and target arrays"},
	{NULL}
};

static PyMemberDef Trainer_members[] = {
	{NULL}
};

static PyTypeObject TrainerType = {
	PyObject_HEAD_INIT(NULL)
	    0,			/*ob_size */
	"ineptron.Trainer",	/*tp_name */
	sizeof(Trainer),	/*tp_basicsize */
	0,			/*tp_itemsize */
	(destructor) Trainer_dealloc,	/*tp_dealloc */
	0,			/*tp_print */
	0,			/*tp_getattr */
	0,			/*tp_setattr */
	0,			/*tp_compare */
	0,			/*tp_repr */
	0,			/*tp_as_number */
	0,			/*tp_as_sequence */
	0,			/*tp_as_mapping */
	0,			/*tp_hash */
	0,			/*tp_call */
	0,			/*tp_str */
	0,			/*tp_getattro */
	0,			/*tp_setattro */
	0,			/*tp_as_buffer */
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,	/*tp_flags */
	Trainer_doc,		/* tp_doc */
	0,			/* tp_traverse */
	0,			/* tp_clear */
	0,			/* tp_richcompare */
	0,			/* tp_weaklistoffset */
	0,			/* tp_iter */
	0,			/* tp_iternext */
	Trainer_methods,	/* tp_methods */
	Trainer_members,	/* tp_members */
	Trainer_getsetters,	/* tp_getset */
	0,			/* tp_base */
	0,			/* tp_dict */
	0,			/* tp_descr_get */
	0,			/* tp_descr_set */
	0,			/* tp_dictoffset */
	(initproc) Trainer_init,	/* tp_init */
	0,			/* tp_alloc */
	Trainer_new,		/* tp_new */
};

/* top level functions */

static PyObject *
Function_enable_fp_exceptions(Trainer * self, PyObject * nothing)
{
	feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
	return Py_BuildValue("");
}

/* bindings for top_level */

static PyMethodDef top_level_functions[] = {
	{"enable_fp_exceptions", (PyCFunction) Function_enable_fp_exceptions,
	 METH_NOARGS, "turn on some floating point exceptions"},
	{NULL}
};

#ifndef PyMODINIT_FUNC
#define PyMODINIT_FUNC void
#endif
PyMODINIT_FUNC
initineptron(void)
{
	if (PyType_Ready(&TrainerType) < 0) {
		return;
	}

	PyObject *m = Py_InitModule3("ineptron", top_level_functions,
				     "Wrapper around RNN for numpy arrays.");

	if (m == NULL) {
		DEBUG("Could not initialise numpy rnn module!");
		return;
	}

	PyExc_IneptError = PyErr_NewException((char *)"ineptron.IneptError",
					      NULL, NULL);
	PyModule_AddObject(m, "IneptError", PyExc_IneptError);

	import_array();
	Py_INCREF(&TrainerType);
	PyModule_AddObject(m, "Trainer", (PyObject *) & TrainerType);
}
