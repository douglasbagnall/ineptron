#include <stddef.h>
#include <limits.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "bits.h"
#include "pbm-dump.h"

/*pbm for bitmap */
void
pbm_dump(const uint64_t *data64,
         const uint width,
         const uint height,
         const char *name)
{
	uint8_t *data = (uint8_t *)data64;
	FILE *fh = fopen(name, "w");
	fprintf(fh, "P4\n%u %u\n", width, height);
	int rx = width;
	uint8_t byte = 0;
	int offset = 0;
	for (int i = 0; i < width * height;) {
		byte = data[i / 8];
		byte <<= offset;
		byte |= data[i / 8 + 1] >> (8 - offset);
		putc(~byte, fh);
		if (rx < 8) {
			offset = (offset + rx) & 7;
			i += rx;
			rx = width;
		} else {
			i += 8;
			rx -= 8;
		}
	}

	fflush(fh);
	fclose(fh);
}
